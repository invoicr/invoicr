import { Action } from '@ngrx/store';

export enum ActionTypes {
    LoadInitialState = '[App] Load initial state from JSON',
    UpdateEditableValue = '[Invoice editor] Update editable value',
    SelectField = '[Invoice editor] Select field',
    MoveToNextField = '[Invoice editor] Move to next field',
    MoveToPreviousField = '[Invoice editor] Move to previous field'
}

export class LoadInitialStateFromJson implements Action {
    readonly type =  ActionTypes.LoadInitialState;

    constructor(public payload: string) {}
}

export class UpdateEditableValue implements Action {
    readonly type = ActionTypes.UpdateEditableValue;

    constructor(public field: string, public value: string) {}
}

export class SelectField implements Action {
    readonly type = ActionTypes.SelectField;

    constructor(public key: string) { }
}

export class MoveToNextField implements Action {
    readonly type = ActionTypes.MoveToNextField;
}

export class MoveToPreviousField implements Action {
    readonly type = ActionTypes.MoveToPreviousField;
}


export type ActionsUnion = LoadInitialStateFromJson
| UpdateEditableValue
| SelectField
| MoveToNextField
| MoveToPreviousField;

