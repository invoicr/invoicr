import { OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';

export class BaseComponent implements OnDestroy {
    rxDestroy = new Subject<void>();

    ngOnDestroy(): void {
        this.rxDestroy.next();
    }
}
