import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EditableTextComponent } from './editable-text/editable-text.component';
import { InvoiceEditorComponent } from './invoice-editor/invoice-editor.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { TopMenuComponent } from './top-menu/top-menu.component';
import { FooterComponent } from './footer/footer.component';
import { StoreModule} from '@ngrx/store';
import { invoiceEditorReducer } from './reducers';

@NgModule({
  declarations: [
    AppComponent,
    EditableTextComponent,
    InvoiceEditorComponent,
    PrivacyPolicyComponent,
    TopMenuComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    StoreModule.forRoot({invoiceEditor: invoiceEditorReducer}),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
