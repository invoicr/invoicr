import { ActionsUnion, ActionTypes } from '../actions';

export interface State {
  _currently_editing: string|null;

  client_name: string;
  client_address_line_1: string;
  client_address_line_2: string;
  client_address_line_3: string;
  client_vat_id: string;
  contractor_name: string;
  contractor_address_line_1: string;
  contractor_address_line_2: string;
  contractor_address_line_3: string;
  contractor_vat_id: string;
  contractor_www: string;
  contractor_email: string;
  invoice_info_nr: string;
  invoice_info_issued_date: string;
  invoice_info_services_rendered_date: string;
  invoice_info_due_date: string;
  totals_tax_explanation: string;
  totals_subtotal: string;
  totals_total: string;
  totals_vat: string;
  footer_payment_method: string;
  footer_signature: string;
  footer_ending_line: string;
}

export const initialState: State = {
  _currently_editing: null,

  contractor_name: 'Contractor name',
  contractor_address_line_1: 'Street 12',
  contractor_address_line_2: '12345 Dallas, TX',
  contractor_address_line_3: 'US',
  contractor_vat_id: '',
  contractor_www: 'https://drola.si',
  contractor_email: 'matjaz@matjazdrolc.eu',
  client_name: 'Client name',
  client_address_line_1: 'Street 12',
  client_address_line_2: '12345 Dallas, TX',
  client_address_line_3: 'US',
  client_vat_id: '',
  invoice_info_nr: 'Nr. 16/2018',
  invoice_info_issued_date: '2018/09/14',
  invoice_info_services_rendered_date: '2018/09/13',
  invoice_info_due_date: '2018/10/15',
  // TODO: items: [],
  // tslint:disable-next-line:max-line-length
  totals_tax_explanation: 'Services subject to the reverse charge - VAT to be accounted for by the recipient as per Article 196 of Council Directive 2006/112/EC.',
  totals_subtotal: '1.100 EUR',
  totals_total: '1.100 EUR',
  totals_vat: '0 %',
  footer_payment_method: 'SWIFT SI56 1234 1234 1234',
  footer_signature: 'Firstname Lastname',
  footer_ending_line: 'Looking forward to future cooperation'
};


const listOfFields = [
  'contractor_name',
  'contractor_address_line_1',
  'contractor_address_line_2',
  'contractor_address_line_3',
  'contractor_vat_id',
  'contractor_www',
  'contractor_email',
  'client_name',
  'client_address_line_1',
  'client_address_line_2',
  'client_address_line_3',
  'client_vat_id',
  'invoice_info_nr',
  'invoice_info_issued_date',
  'invoice_info_due_date',
  'invoice_info_services_rendered_date',
  'totals_subtotal',
  'totals_vat',
  'totals_total',
  // 'totals_tax_explanation',
  'footer_payment_method',
  'footer_signature',
  'footer_ending_line'
];

export function invoiceEditorReducer(
  state = initialState,
  action: ActionsUnion
): State {
  let nextEditing: number|null;
  const currentlyEditingIndex = state._currently_editing !== null ? listOfFields.indexOf(state._currently_editing) : -1;

  switch (action.type) {
    case ActionTypes.LoadInitialState:
      const currStateJson = JSON.stringify(state);
      if (currStateJson !== action.payload) {
        return {...JSON.parse(action.payload), _currently_editing: null} as State;
      } else {
        return state;
      }
    case ActionTypes.SelectField:
      return {...state, _currently_editing: action.key};
    case ActionTypes.MoveToNextField:
      if (currentlyEditingIndex < 0) {
        nextEditing = 0;
      } else if (currentlyEditingIndex >= listOfFields.length - 1) {
        nextEditing = 0;
      } else {
        nextEditing = currentlyEditingIndex + 1;
      }
      return {...state, _currently_editing: listOfFields[nextEditing]};
    case ActionTypes.MoveToPreviousField:
      if (currentlyEditingIndex < 0) {
        nextEditing = listOfFields.length - 1;
      } else if (currentlyEditingIndex === 0) {
        nextEditing = listOfFields.length - 1;
      } else {
        nextEditing = currentlyEditingIndex - 1;
      }
      return {...state, _currently_editing: listOfFields[nextEditing]};
    case ActionTypes.UpdateEditableValue:
      const newState = {...state};
      newState[action.field] = action.value;
      return newState;
    default:
      return state;
  }
}
