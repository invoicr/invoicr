import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { State } from '../reducers';
import { LoadInitialStateFromJson } from '../actions';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-invoice-editor',
  templateUrl: './invoice-editor.component.html',
  styleUrls: ['./invoice-editor.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class InvoiceEditorComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router, private state: Store<{invoiceEditor: State}>) { }

  ngOnInit() {
    // Load state from queryParams
    this.route.queryParams.pipe(take(1)).subscribe((params) => {
      if (params.state) {
        this.state.dispatch(new LoadInitialStateFromJson(params.state));
      }
    });

    this.state.select('invoiceEditor').subscribe((state) => {
      this.router.navigate([''], {queryParams: {state: JSON.stringify(state)}});
    });

    this.state.subscribe(console.log);
  }
}
