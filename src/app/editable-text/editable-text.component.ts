import { Component, ElementRef, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { map, takeUntil } from 'rxjs/operators';

import { SelectField, UpdateEditableValue, MoveToNextField } from '../actions';
import { State } from '../reducers';
import { BaseComponent } from '../util/base.component';

@Component({
  selector: 'app-editable-text',
  templateUrl: './editable-text.component.html',
  styleUrls: ['./editable-text.component.scss']
})
export class EditableTextComponent extends BaseComponent implements OnInit {
  @Input()
  key: string;

  inputVisible = false;
  value: string;
  originalValue: string;

  constructor(private elementRef: ElementRef, private state: Store<{invoiceEditor: State}>) {
    super();
  }

  ngOnInit() {
    this.state.pipe(map(v => v.invoiceEditor[this.key] as string), takeUntil(this.rxDestroy)).subscribe((v) => {
      this.value = v;
      this.originalValue = v;
    });
    this.state.pipe(map(v => v.invoiceEditor._currently_editing), takeUntil(this.rxDestroy)).subscribe((currentlyEditing: string) => {
      if (currentlyEditing === this.key && !this.inputVisible) {
        this.inputVisible = true;
        setTimeout(() => {
      const elem: HTMLInputElement = this.elementRef.nativeElement.getElementsByTagName('input')[0];
      elem.focus();
      elem.select();
    }, 0);
      } else {
        this.inputVisible = false;
      }
    });
  }

  onClick($evt) {
    $evt.preventDefault();
    this.state.dispatch(new SelectField(this.key));
  }

  onSubmit($evt) {
    this.inputVisible = false;
    if (this.originalValue !== this.value) {
      this.state.dispatch(new UpdateEditableValue(this.key, this.value));
    }
    this.state.dispatch(new MoveToNextField());
  }
}
