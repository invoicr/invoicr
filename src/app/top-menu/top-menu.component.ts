import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-top-menu',
  templateUrl: './top-menu.component.html',
  styleUrls: ['./top-menu.component.scss']
})
export class TopMenuComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  downloadPdf() {
    const params = window.location.search;
    window.open("https://renderer.invoicr.app/render/invoice.pdf" + params);
  }

}
